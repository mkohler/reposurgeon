#!/bin/bash
#
# liftcheck - compare reposurgeon lift of a Subversion repo with a checkout
#
# The -v option reveals executed subcommands.
# The -u option enables context-diffing in the compare operation
# The -i option disables the normal suppression of ignore comparisons
# The -d option disables removal of the generated repositories after the run
# The -a option enables progress messages from the Subversion loader.
# The -s option enables display of common files
#
set -e

repotoolopt=""
debug=no
svnquiet=-q
while getopts adisuv opt
do
    case $opt in
	d) debug=yes;;
	a) svnquiet="";;
	i|s|u) compareopt="$compareopt -$opt";;
	v) repotoolopt="$repotoolopt -$opt";;
	u) compareopt="$compareopt -$opt";;
    esac
done
shift $(($OPTIND - 1))

if [ -z "$1" ]
then
    echo "liftcheck: an argument file (a Subversion dump) is required."
    exit 1
fi

stem=liftcheck$$

rm -fr liftcheck[0123456789]*

if [ $debug = no ]
then
    trap "rm -fr ${stem} ${stem}-checkout ${stem}-git ${stem}*~" 0 2 15
fi

# Make a Subversion repo from the dump
svn-to-svn ${svnquiet} -c <$1 ${stem}

# Make a git repo from the dump using reposurgeon
reposurgeon "read <$1" "prefer git" "rebuild ${stem}-git"

# Compare the original with the lift
repotool ${repotoolopt} compare-all ${compareopt} ${stem}-checkout ${stem}-git

if [ $debug != no ]
then
    ls -d liftcheck[0123456789]*
fi

# end
